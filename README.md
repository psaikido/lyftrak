# lyftrak

* A journaling app built on electron  
* Multiple, separate data stores    
* The data doesn't go online  
* Embedded data means it is all self-contained    
* You can keep a daily log of health stats of 'energy', 'pain and 'mood'  
* Stats are configurable
* Backup/restore facility  

NB: the name 'lyftrak' is cutspel (cutspel.com ftw!) for 'life track' in trad spelling.

## Installation

Depends on having node installed - https://nodejs.org/

From the command line:  
git clone git@bitbucket.org:psaikido/lyftrak.git  
cd lyftrak  
npm install  
npm start  
