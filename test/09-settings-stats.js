/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('settings stats', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('settings: page has data', function () {
    return this.app.client
      .click('#accesskey_t')
      .click('#tbl-data-stores-0 .rowStatTrigger td')
      .waitForExist('#cell-ds-0-0-col_1')
      .getValue('#cell-ds-0-0-col_1 .inpName')
      .then((actual) => {
        t.assert.strictEqual(actual, 'energy')
      })
      .getValue('#cell-ds-0-0-col_2 .inpStart')
      .then((actual) => {
        t.assert.strictEqual(actual, '0')
      })
      .getValue('#cell-ds-0-0-col_3 .inpEnd')
      .then((actual) => {
        t.assert.strictEqual(actual, '100')
      })
      .$('#cell-ds-0-0-col_4 .chk.time').getAttribute('checked')
      .then((actual) => {
        t.assert.strictEqual(actual, null)
      })
      .$('#cell-ds-0-0-col_5 .chk.enabled').getAttribute('checked')
      .then((actual) => {
        t.assert.strictEqual(actual, 'true')
      })
      .setValue('#cell-ds-0-3-col_1 .inpName', 'spong')
      .setValue('#cell-ds-0-3-col_2 .inpStart', '0')
      .setValue('#cell-ds-0-3-col_3 .inpEnd', '10')
      .click('#cell-ds-0-3-col_5 .chk.enabled')
      .click('#btnSaveDataStores')
      .click('#tbl-data-stores-0 .rowStatTrigger td')
      .waitForExist('#cell-ds-0-3-col_1')
      .getValue('#cell-ds-0-3-col_1 .inpName')
      .then((actual) => {
        t.assert.strictEqual(actual, 'spong')
      })
  })

  it('settings: disable a stat', function () {
    return this.app.client
      .click('#accesskey_t')
      .waitForExist('#cell-ds-0-0-col_5')
      .click('#tbl-data-stores-0 .rowStatTrigger td')
      .click('#cell-ds-0-0-col_5 .chk.enabled')
      .click('#btnSaveDataStores')
      .waitForExist('#cell-ds-0-0-col_5')
      .click('#accesskey_l')
      .waitForExist('#titleDate_1')
      .isExisting('#energy_1')
      .then((actual) => {
        t.assert.strictEqual(actual, false)
      })
      .click('#accesskey_t')
      .click('#tbl-data-stores-0 .rowStatTrigger td')
      .waitForExist('#cell-ds-0-0-col_5')
      .click('#cell-ds-0-0-col_5 .chk.enabled')
      .click('#btnSaveDataStores')
      .waitForExist('#cell-ds-0-0-col_5')
      .click('#accesskey_l')
      .waitForExist('#titleDate_1')
      .getText('#energy_1')
      .then((actual) => {
        t.assert.strictEqual(actual, 'energy: 100 / 100')
      })
  })

  it('settings: use a time stat', function () {
    return this.app.client
      .click('#accesskey_t')
      .click('#tbl-data-stores-0 .rowStatTrigger td')
      .waitForExist('#cell-ds-0-3-col_1')
      .setValue('#cell-ds-0-3-col_1 .inpName', 'zazen')
      .setValue('#cell-ds-0-3-col_2 .inpStart', '0')
      .setValue('#cell-ds-0-3-col_3 .inpEnd', '1440')
      .click('#cell-ds-0-3-col_4 .chk.time')
      .click('#btnSaveDataStores')
      .waitForExist('#cell-ds-0-3-col_5')
      .click('#accesskey_l')
      .waitForExist('#titleDate_1')
      .click('#btnEdit_1')
      .pause(500)
      .selectByIndex('#inp_zazen_hours', 1)
      .selectByIndex('#inp_zazen_mins', 4)
      .click('#btnSave')
      .waitForExist('#titleDate_1')
      .getText('#zazen_1')
      .then((actual) => {
        t.assert.strictEqual(actual, 'zazen: 01:04')
      })
  })
})
