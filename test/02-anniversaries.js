/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('anniversaries', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('anniversaries: read first row', function () {
    return this.app.client
      .click('#accesskey_i')
      .waitForExist('#row_0 .inpDesc')
      .getValue('#row_0 .inpDesc')
      .then((actual) => {
        const textFound = actual.indexOf('default') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .getValue('#row_0 .inpDate')
      .then((actual) => {
        const textFound = actual.indexOf('1970-01-01') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('anniversaries: new row', function () {
    return this.app.client
      .click('#accesskey_i')
      .waitForExist('#row_1 .inpDesc')
      .setValue('#row_1 .inpDesc', 'thing')
      .setValue('#row_1 .inpDate', '2020-01-27')
      .click('#btnSave')
      .waitForExist('#row_1 .inpDesc')
      .getValue('#row_1 .inpDesc')
      .then((actual) => {
        const textFound = actual.indexOf('thing') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .getValue('#row_1 .inpDate')
      .then((actual) => {
        const textFound = actual.indexOf('2020-01-27') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })
})
