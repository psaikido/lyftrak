/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('backup and restore', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('backups: check page', function () {
    return this.app.client
      .click('#accesskey_b')
      .waitForExist('.left h4')
      .getText('.left h4')
      .then((actual) => {
        const textFound = actual.indexOf('backup & encrypt') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })
})
