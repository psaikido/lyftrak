/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('search', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('search: text', function () {
    return this.app.client
      .click('#accesskey_r')
      .waitForExist('#search h3')
      .getText('#search h3')
      .then((actual) => {
        t.assert.strictEqual(actual, 'search')
      })
      .setValue('#inpSearchTerm', 'spong')
      .click('#btnSave')
      .waitForExist('#entryWrap_K48vSeHc4Iypi9pB')
      .getText('#entryWrap_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('search: date range', function () {
    return this.app.client
      .click('#accesskey_r')
      .waitForExist('#inpDateStart')
      .setValue('#inpDateStart', '2020-01-01')
      .setValue('#inpDateEnd', '2020-01-05')
      .click('#btnSave')
      .waitForExist('#entry_1')
      .getText('#entry_1')
      .then((actual) => {
        const textFound = actual.indexOf('Welcome to lyftrak') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .getText('#entryWrap_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('search: id', function () {
    return this.app.client
      .click('#accesskey_r')
      .waitForExist('#inpSearchId')
      .setValue('#inpSearchId', 'K48vSeHc4Iypi9pB')
      .click('#btnSave')
      .waitForExist('#entryWrap_K48vSeHc4Iypi9pB')
      .getText('#entryWrap_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('search: tags', function () {
    return this.app.client
      .click('#accesskey_r')
      .pause(200)
      .element('#inpSearchTag')
      .selectByVisibleText('spong')
      .click('#btnSave')
      .waitForExist('#entryWrap_K48vSeHc4Iypi9pB')
      .getText('#entryWrap_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('#spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('search: media pix', function () {
    return this.app.client
      .click('#accesskey_r')
      .waitForExist('#inpSearchMediaPix')
      .click('#inpSearchMediaPix')
      .click('#btnSave')
      .waitForExist('#message')
      .getText('#message')
      .then((actual) => {
        const textFound = actual.indexOf('no results') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('search: media vids', function () {
    return this.app.client
      .click('#accesskey_r')
      .waitForExist('#inpSearchMediaVids')
      .click('#inpSearchMediaVids')
      .click('#btnSave')
      .waitForExist('#message')
      .getText('#message')
      .then((actual) => {
        const textFound = actual.indexOf('no results') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })
})
