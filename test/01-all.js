/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('all', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('entries present', function () {
    return this.app.client
      .click('#accesskey_a')
      .waitForExist('#entry_1')
      .getText('#entry_1')
      .then(function (actual) {
        const textFound = actual.indexOf('Welcome to lyftrak') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .getText('#entryWrap_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('edit buttons', function () {
    return this.app.client
      .click('#accesskey_a')
      .waitForExist('#btnEdit_K48vSeHc4Iypi9pB')
      .click('#btnEdit_K48vSeHc4Iypi9pB')
      .pause(500)
      .getText('#titleId')
      .then((actual) => t.assert.strictEqual(actual, 'K48vSeHc4Iypi9pB'))
  })

  it('view buttons', function () {
    return this.app.client
      .click('#accesskey_a')
      .waitForExist('#btnView_1')
      .click('#btnView_1')
      .waitForExist('#titleDate_1')
      .getText('#titleDate_1')
      .then((actual) => t.assert.strictEqual(actual, '2020-01-04 15:00'))
  })
})
