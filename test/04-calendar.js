/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('calendar', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('calendar: two dates hilighted', function () {
    return this.app.client
      .click('#accesskey_c')
      .waitForExist('#day_2020_0_0_0_3')
      .getHTML('#day_2020_0_0_0_3')
      .then((actual) => {
        const textFound = actual.indexOf('hilight') !== -1
        t.assert.strictEqual(textFound, false)
      })
      .getHTML('#day_2020_0_0_0_4')
      .then((actual) => {
        const textFound = actual.indexOf('hilight') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .getHTML('#day_2020_0_0_0_5')
      .then((actual) => {
        const textFound = actual.indexOf('hilight') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })
})
