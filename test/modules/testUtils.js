/* eslint-env mocha */
const jetpack = require('fs-jetpack')

module.exports = class TestUtils {
  constructor () {
    const homeDir = process.env.HOME
    this.homeDir = homeDir.toString() + '/lyftrak/'
    this.Application = require('spectron').Application
    this.assert = require('assert').strict
    this.electronPath = require('electron')
    this.path = require('path')
  }

  createSpectron () {
    return new this.Application({
      path: this.electronPath,
      args: [this.path.join(__dirname, '..', '..')]
    })
  }

  initialise () {
    this._resetFiles('init')
  }

  tidy () {
    this._resetFiles()

    jetpack.remove('user_data/uploads/lyftrak/lake_island.jpg')
    jetpack.remove('user_data/test.json')
  }

  _resetFiles (mode) {
    this._resetFile(mode, 'lyftrak')
    this._resetFile(mode, 'anniversaries')
    this._resetFile(mode, 'settings')
  }

  _resetFile (mode, file) {
    const src = this._getSourceFile(mode, file)
    const dest = this.homeDir + `user_data/${file}.json`
    jetpack.copy(src, dest, { overwrite: true })
  }

  _getSourceFile (mode, file) {
    // When initialising we just want the x.default.json returned
    if (mode === 'init') {
      return this.homeDir + `user_data/${file}.default.json`
    }

    // When tidying up we want to reset from a user's own data
    // if it exists and only then see if there is an x.default.json
    const userFile = this.homeDir + `user_data/${file}.user.json`

    if (jetpack.exists(userFile) === 'file') {
      return userFile
    } else {
      return this.homeDir + `user_data/${file}.default.json`
    }
  }
}
