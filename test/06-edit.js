/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()
const dateUtils = require('../src/modules/dateUtils.js')

describe('edit journal entry', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('edit journal entry', function () {
    return this.app.client
      .waitForExist('#btnEdit_1')
      .click('#btnEdit_1')
      .pause(500)
      .setValue('#inpTitleDate', '2020-01-05 16:00')
      .setValue('.jodit_wysiwyg', 'blah blah blah')
      .setValue('#inpTags', 'spong')
      .selectByIndex('#inp_energy', 50)
      .selectByIndex('#inp_pain', 1)
      .selectByIndex('#inp_mood', 9)
      .click('#btnSave')
      .waitForExist('#titleDate_1')
      .getText('#titleDate_1')
      .then((actual) => {
        t.assert.strictEqual(actual, '2020-01-05 16:00')
      })
      .getText('#entry_1')
      .then((actual) => {
        t.assert.strictEqual(actual, 'blah blah blah\n#spong\n ')
      })
      .getText('#energy_1 h6')
      .then((actual) => {
        t.assert.strictEqual(actual, 'energy: 50 / 100')
      })
      .getText('#pain_1 h6')
      .then((actual) => {
        t.assert.strictEqual(actual, 'pain: 1 / 10')
      })
      .getText('#mood_1 h6')
      .then((actual) => {
        t.assert.strictEqual(actual, 'mood: 9 / 10')
      })
  })

  it('upload', function () {
    var fileToUpload = t.path.join(__dirname, './img/lake_island.jpg')

    return this.app.client
      .click('#btnEdit_1')
      .waitForExist('#inpFile')
      .chooseFile('#inpFile', fileToUpload)
      .pause(1000)
      .click('#btnSave')
      .pause(1000)
      .getHTML('#pix_1_1 img')
      .then((actual) => {
        const textFound = actual.indexOf('lake_island.jpg') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('new page', function () {
    return this.app.client
      .click('#accesskey_n')
      .waitForExist('#inpTitleDate')
      .getValue('#inpTitleDate')
      .then((actual) => {
        const nowMysql = dateUtils.getNowMysql()
        t.assert.strictEqual(actual, nowMysql)
      })
  })
})
