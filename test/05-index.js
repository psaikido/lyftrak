/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('journal entry', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('gets the title', function () {
    return this.app.client
      .getTitle()
      .then((actual) => {
        t.assert.strictEqual(actual, 'lyftrak - demo repository')
      })
  })

  it('latest page: get title date', function () {
    return this.app.client
      .waitForExist('#titleDate_1')
      .getText('#titleDate_1')
      .then((actual) => {
        t.assert.strictEqual(actual, '2020-01-04 15:00')
      })
  })

  it('latest page: get main text', function () {
    return this.app.client
      .getText('#entry_1')
      .then((actual) => {
        const textFound = actual.indexOf('Welcome to lyftrak') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })

  it('latest page: navigation', function () {
    return this.app.client
      .click('.btnEarliest')
      .waitForExist('#entry_K48vSeHc4Iypi9pB')
      .getText('#entry_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .click('.btnLatest')
      .waitForExist('#entry_1')
      .getText('#entry_1')
      .then((actual) => {
        const textFound = actual.indexOf('Welcome to lyftrak') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .click('.btnPrevious')
      .waitForExist('#entry_K48vSeHc4Iypi9pB')
      .getText('#entry_K48vSeHc4Iypi9pB')
      .then((actual) => {
        const textFound = actual.indexOf('spong') !== -1
        t.assert.strictEqual(textFound, true)
      })
      .click('.btnNext')
      .waitForExist('#entry_1')
      .getText('#entry_1')
      .then((actual) => {
        const textFound = actual.indexOf('Welcome to lyftrak') !== -1
        t.assert.strictEqual(textFound, true)
      })
  })
})
