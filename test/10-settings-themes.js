/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('settings themes', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('settings: themes exist', function () {
    return this.app.client
      .click('#accesskey_t')
      .waitForExist('#themes h3')
      .getText('#themes h3')
      .then((actual) => {
        t.assert.strictEqual(actual, 'themes')
      })
      .getText('#theme_0_col_1')
      .then((actual) => {
        t.assert.strictEqual(actual, 'lyftrak')
      })
      .getText('#theme_1_col_1')
      .then((actual) => {
        t.assert.strictEqual(actual, 'plain')
      })
  })

  it('settings: change theme', function () {
    return this.app.client
      .click('#accesskey_t')
      .waitForExist('#themes h3')
      .click('#theme_1_col_2 .chk')
      .click('#btnSaveThemes')
      .waitForExist('#themes h3')
  })
})
