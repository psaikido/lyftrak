/* eslint-env mocha */
const T = require('./modules/testUtils.js')
const t = new T()

describe('settings data-stores', function () {
  this.timeout(10000)

  before(async function () {
    t.initialise()
    this.app = t.createSpectron()
    await this.app.start()
  })

  after(function () {
    if (this.app && this.app.isRunning()) {
      t.tidy()
      return this.app.stop()
    }
  })

  it('data-stores', function () {
    return this.app.client
      .click('#accesskey_t')
      .waitForExist('#data-stores h3')
      .getText('#data-stores h3')
      .then((actual) => {
        t.assert.strictEqual(actual, 'data stores')
      })
      .setValue('#tbl-data-stores-1 .inpDesc', 'test')
      .setValue('#tbl-data-stores-1 .inpData', 'test.json')
      .click('#btnSaveDataStores')
      .waitForExist('#data-stores h3')
      .getValue('#tbl-data-stores-1 .inpDesc')
      .then((actual) => {
        t.assert.strictEqual(actual, 'test')
      })
      .pause(200)
      .click('#chkSelected-1')
      .click('#btnSaveDataStores')
      .waitForExist('#data-stores h3')
      .getTitle()
      .then((actual) => {
        t.assert.strictEqual(actual, 'lyftrak - test')
      })
  })
})
