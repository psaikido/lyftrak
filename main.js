/* eslint-env */
const electron = require('electron')
const ipcMain = electron.ipcMain
const path = require('path')
const app = electron.app
const BrowserWindow = electron.BrowserWindow
require('./src/includes/menu-data')

try {
  require('electron-reloader')(module, {
    ignore: /user_data/g
  })

  require('dotenv').config()
} catch (e) {
  console.log(e)
}

let mainWindow
let idCurrent = 0

function createWindow () {
  mainWindow = new BrowserWindow({
    width: 800,
    height: 700,
    webPreferences: {
      preload: path.join(__dirname, 'preload.js'),
      nodeIntegration: true,
      webSecurity: true,
      icon: path.join(__dirname, 'assets/img/favicon.png')
    },
    show: false,
    frame: false,
    backgroundColor: '#073642'
  })

  mainWindow.loadURL(`file://${__dirname}/src/index.html`)
  mainWindow.setIcon(path.join(__dirname, 'assets/img/favicon.png'))

  mainWindow.on('closed', function () {
    mainWindow = null
  })

  mainWindow.on('ready-to-show', () => {
    mainWindow.show()
  })

  ipcMain.on('editEntry', function (event, args) {
    idCurrent = args
    mainWindow.loadURL(`file://${__dirname}/src/edit.html?id=` + idCurrent)
  })

  ipcMain.on('getEntryFromId', function (event, args) {
    idCurrent = args
    mainWindow.loadURL(`file://${__dirname}/src/index.html?id=` + idCurrent)
  })

  ipcMain.on('nav', function (event, src) {
    mainWindow.loadURL(`file://${__dirname}/${src}`)
  })
}

app.on('ready', function () {
  createWindow()
})

app.on('window-all-closed', function () {
  if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function () {
  if (mainWindow === null) createWindow()
})
