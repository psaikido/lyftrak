// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
window.addEventListener('DOMContentLoaded', () => {
  const replaceText = (selector, text) => {
    const element = document.getElementById(selector)
    if (element) element.innerText = text
  }

  for (const type of ['chrome', 'node', 'electron']) {
    replaceText(`${type}-version`, process.versions[type])
  }
})

// get and set global settings
const settingsUtils = require('./src/modules/settingsUtils')
global.settings = settingsUtils.getAllSettings()

// create upload directory if it doesn't exist
const jetpack = require('fs-jetpack')
jetpack.dir(global.settings.dataStore.uploadDir)
