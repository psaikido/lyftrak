/* eslint-env */
const dateUtils = require('./modules/dateUtils')
const tagUtils = require('./modules/tagUtils')
const statsUtils = require('./modules/settingsUtils')
const mediaUtils = require('./modules/mediaUtils')
const jetpack = require('fs-jetpack')
const Nedb = require('./modules/db-nedb')
var db = new Nedb()
let editor

const urlParams = new URLSearchParams(window.location.search)
var idCurrent = urlParams.get('id')
var mode = ''

if (idCurrent) {
  mode = 'edit'
  editEntry()
} else {
  mode = 'new'
  newRecord()
}

function newRecord () {
  doTags()
  doStats()
}

function editEntry () {
  db.getOne(idCurrent, function (error, results) {
    if (error) {
      $('.message').text(error.code)
    } else {
      if (results.length < 1) { return }

      var row = results[0]

      $('.entry').attr('id', 'entry_' + idCurrent)

      var entryDateHuman = dateUtils.convertUnixToMysql(row.dateTime)
      $('#inpTitleDate').val(entryDateHuman)

      $('#titleId').html(row._id)
      idCurrent = row._id
      editor.value = row.textEntry
      $('#inpTags').val(row.tags)
      mediaUtils.doMedia(idCurrent, row.pix, 'edit')
    }
  })

  doTags()
  doStats()
}

function doStats () {
  const stats = statsUtils.getStats(idCurrent, db)
  stats.then(function (results) {
    for (var i = 0; i < global.settings.dataStore.stats.length; i++) {
      var stat = global.settings.dataStore.stats[i]
      doStat(stat, results)
    }
  })
}

function doStat (stat, results) {
  if (stat.enabled) {
    var currentVal = 0

    if (results) {
      for (var s in results.stats) {
        if (stat.name === s) {
          currentVal = results.stats[s]
          break
        }
      }
    }

    $(`<h6>${stat.name}</h6>`)
      .appendTo('.stats')

    if (stat.time) {
      var hours = Math.floor(currentVal / 60)
      var mins = currentVal % 60

      // hours
      $(`<select id="inp_${stat.name}_hours" class="time">`)
        .appendTo('.stats')

      for (var h = 0; h <= 24; h++) {
        var selected = ''
        if (h === parseInt(hours)) {
          selected = 'selected'
        }

        $(`#inp_${stat.name}_hours`)
          .append(
            $(`<option ${selected}>`)
              .val(h)
              .text(h)
          )
      }

      // minutes
      $(`<select id="inp_${stat.name}_mins" class="time">`)
        .appendTo('.stats')

      for (var m = 0; m <= 59; m = m + 5) {
        selected = ''
        if (m === parseInt(mins)) {
          selected = 'selected'
        }

        $(`#inp_${stat.name}_mins`)
          .append(
            $(`<option ${selected}>`)
              .val(m)
              .text(m)
          )
      }
    } else {
      $(`<select id="inp_${stat.name}">`)
        .appendTo('.stats')
        .add('<option value="">')

      for (var i = stat.start; i <= stat.end; i++) {
        selected = ''

        if (mode === 'edit') {
          if (i === parseInt(currentVal)) {
            selected = 'selected'
          }
        }

        $(`#inp_${stat.name}`)
          .append(
            $(`<option ${selected}>`)
              .val(i)
              .text(i)
          )
      }
    }
  }
}

function doTags () {
  const tags = tagUtils.getAllTags(db)
  tags.then(function (results) {
    for (var i = 0; i < results.length; i++) {
      $('#inpTag')
        .append(
          $('<option />')
            .val(results[i])
            .text(results[i])
        )
    }
  }, function (err) {
    $('.message').text(err)
  })
}

$('#btnSave').on('click', () => {
  var inpDate = $('#inpTitleDate').val()
  if (!inpDate) {
    $('.message').text('please enter a valid date')
    return
  }

  var rec

  if (mode === 'edit') {
    rec = buildEntryObj()
    rec.then(function (results) {
      var entryRec = results

      db.update(entryRec, function (error, results) {
        if (error) {
          console.log(error)
        } else {
          console.log(results)
        }
      })

      ipcRenderer.send('getEntryFromId', idCurrent)
    })
  } else {
    rec = buildEntryObj()
    rec.then(function (results) {
      var entryRec = results

      db.insert(entryRec, function (error, results) {
        if (error) {
          console.log(error)
        } else {
          ipcRenderer.send('getEntryFromId', results._id)
        }
      })
    })
  }
})

$('#btnDelete').on('click', () => {
  db.remove(idCurrent, function (error) {
    if (error) {
      console.log(error)
    } else {
      ipcRenderer.send('nav', './src/index.html')
    }
  })
})

function buildEntryObj () {
  var inpDate = $('#inpTitleDate').val()
  var dateTimePage = dateUtils.convertMysqlToUnix(inpDate)
  var rec

  return new Promise(function (resolve, reject) {
    var data = $('#inpFile').prop('files')[0]
    if (data) {
      upload(data)
    }

    var pixString = getPixString(data)
    pixString.then(function (results) {
      var pixUpdateString = results

      var statValues = buildStatObj()

      if (mode === 'edit') {
        var idCurrent = $('#titleId').prop('textContent')

        rec = {
          _id: idCurrent,
          dateTime: dateTimePage,
          stats: statValues,
          pix: pixUpdateString,
          tags: $('#inpTags').val(),
          textEntry: $('#inpText').val()
        }
      } else {
        rec = {
          dateTime: dateTimePage,
          stats: statValues,
          pix: pixUpdateString,
          tags: $('#inpTags').val(),
          textEntry: $('#inpText').val()
        }
      }

      resolve(rec)
    })
  })
}

function buildStatObj () {
  var statObj = []

  $.each($('.stats select'), function (key, val) {
    var statName

    if ($(val).prop('class') !== 'time') {
      statName = $(val)
        .prop('id')
        .replace('inp_', '')

      var statVal = $(val).val()
    }

    statObj[statName] = statVal
  })

  $.each($('.stats select.time'), function (key, val) {
    var minsTot
    var statNameOrig = $(val).prop('id').split('_')

    if (statNameOrig[2] === 'hours') {
      minsTot = Math.floor($(val).val() * 60)
      statObj[statNameOrig[1]] = minsTot
    }

    if (statNameOrig[2] === 'mins') {
      minsTot = parseInt($(val).val())
      statObj[statNameOrig[1]] = parseInt(statObj[statNameOrig[1]]) + minsTot
    }
  })

  statObj = $.extend({}, statObj)

  return statObj
}

$('#btnView').on('click', () => {
  ipcRenderer.send('getEntryFromId', idCurrent)
})

$('#inpTag').on('change', function () {
  var chosenTag = $('#inpTag').val()
  var existingTags = $('#inpTags').val()
  var resultText = ''

  if (existingTags) {
    resultText = existingTags + ',' + chosenTag
  } else {
    resultText = chosenTag
  }

  $('#inpTags').val(resultText)
})

function upload (data) {
  const src = jetpack.cwd(data.path)
  var dest = global.settings.dataStore.uploadDir + data.name

  src.copy(data.path, dest)
}

function getPixString (data) {
  return new Promise(function (resolve, reject) {
    var pixUpdateString = ''

    try {
      if (idCurrent) {
        var existingPix = mediaUtils.getExistingPix(idCurrent)
        existingPix.then(function (results) {
          if (results) {
            var existingPixArray = results.split(',')

            for (var x = 0; x < existingPixArray.length; x++) {
              if (pixUpdateString.length > 0) {
                pixUpdateString += ',' + existingPixArray[x]
              } else {
                pixUpdateString = existingPixArray[x]
              }
            }

            if (data) {
              pixUpdateString += ',' + data.name
              resolve(pixUpdateString)
            }
          }

          if (data) {
            pixUpdateString = ',' + data.name
          }

          resolve(pixUpdateString)
        })
      } else {
        if (data) {
          pixUpdateString = data.name
          resolve(pixUpdateString)
        } else {
          resolve('')
        }
      }
    } catch (e) {
      console.log('Failed to save the file!')
    }
  })
}

$('#datepickerIcon').click(function () {
  $('#inpTitleDate').datetimepicker({
    theme: 'dark',
    mask: '9999-19-39 23:59',
    format: 'Y-m-d H:i',
    value: $('#inpTitleDate').val()
  })

  $('#inpTitleDate').datetimepicker('show')
})

$().ready(function () {
  editor = new Jodit('#inpText', {
    theme: 'darkH'
  })

  if (mode === 'new') {
    var entryDateHuman = dateUtils.getNowMysql()
    $('#inpTitleDate').val(entryDateHuman)
  }
})
