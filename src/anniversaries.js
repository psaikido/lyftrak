/* eslint-env */
const electron = require('electron')
const dateFormat = require('dateformat')
const dates = require('../user_data/anniversaries.json')
const jsonUtils = require('./modules/jsonUtils')

buildDisplay(jsonUtils.readAnniversaries(dates.anniversaries))

function buildDisplay (data) {
  $('<span id="tables">')
    .appendTo('#frmAn')

  $.each(data, function (key, val) {
    $(`<div class="row" id="row_${key}">`)
      .appendTo('#tables')

    $('<input type="text" class="inpDesc" />')
      .val(val.desc)
      .appendTo(`#row_${key}`)

    var displayDate = ''

    if (val.mysql.toString() !== 'Invalid Date') {
      displayDate = dateFormat(val.mysql, 'yyyy-mm-dd')
    }

    $(`<input type="text" class="inpDate" id="inpDate_${key}" />`)
      .val(displayDate)
      .appendTo(`#row_${key}`)

    $(`<img src="../assets/img/datepicker-icon.png" id="datepickerIcon_${key}" title="datepicker" />`)
      .on('click', function () {
        dpClick(key)
      })
      .appendTo(`#row_${key}`)
  })

  // boxes for a new record
  $(`<div class="row" id="row_${data.length}">`)
    .appendTo('#tables')

  $('<input type="text" class="inpDesc" />')
    .appendTo(`#row_${data.length}`)

  $(`<input type="text" class="inpDate" id="inpDate_${data.length}" />`)
    .appendTo(`#row_${data.length}`)

  $(`<img src="../assets/img/datepicker-icon.png" id="datepickerIcon_${data.length}" title="datepicker"  />`)
    .on('click', function () {
      dpClick(data.length)
    })
    .appendTo(`#row_${data.length}`)

  // save button
  $('<input type="button" id="btnSave" class="save" value="Save" accesskey="s" title="Alt+s" />')
    .appendTo('#frmAn')
}

function getRows () {
  var recs = []

  $('.row').each(function (key, row) {
    var desc = $(row).find('.inpDesc').val()
    var date = $(row).find('.inpDate').val()
    recs.push({ desc: desc, mysql: date })
  })

  return recs
}

$('#btnSave').on('click', () => {
  var newRows = []
  var rows = getRows()

  $.each(rows, function (key, row) {
    if (
      row.desc !== '' &&
      row.mysql !== '' &&
      row.mysql.toString() !== 'Invalid Date'
    ) {
      newRows.push({
        baseDate: dateFormat(row.mysql, 'yyyy-mm-dd'),
        desc: row.desc
      })
    }
  })

  var newStuff = { anniversaries: newRows }
  jsonUtils.saveData('anniversaries.json', newStuff)

  const localWindow = electron.remote.getCurrentWindow()
  localWindow.reload()
})

function dpClick (key) {
  console.log(key)
  $(`#inpDate_${key}`).datetimepicker({
    theme: 'dark',
    format: 'Y-m-d',
    timepicker: false
  })

  $(`#inpDate_${key}`).datetimepicker('show')
}
