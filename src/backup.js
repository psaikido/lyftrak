/* eslint-env */
const path = require('path')
const userDataDir = path.join(__dirname, '../user_data/')
const zlib = require('zlib')
const gzip = zlib.createGzip()
const jetpack = require('fs-jetpack')
const { dialog } = require('electron').remote
const Dialogs = require('dialogs')
const dialogs = new Dialogs()
const dateUtils = require('./modules/dateUtils')
const timestamp = dateUtils.getNowMysqlShort()
const Crypt = require('./modules/cryptUtils')
const crypt = new Crypt()
const AppendIV = require('./modules/appendInitVector')
const destName = 'lyftrak-' + timestamp + '.gz'
var tar = require('tar-fs')
var gunzip = require('gunzip-maybe')

$('#backup-crp').click(function () {
  dialog.showOpenDialog({
    properties: ['openDirectory']
  }).then(result => {
    if (result.filePaths) {
      encrypt(result.filePaths)
    }
  }).catch(err => {
    console.error(err)
  })
})

function encrypt (destination) {
  $('.right img#waiting')
    .css('display', 'block')

  dialogs.prompt('password', pwd => {
    const destinationFile = path.join(
      destination.toString(),
      destName
    )

    const cipherKey = crypt.getCipherKey(pwd)
    const cipher = crypt.makeCipher(cipherKey)
    const appendInitVect = new AppendIV(crypt.initVect)
    const writeStream = jetpack.createWriteStream(destinationFile)

    tar
      .pack(userDataDir)
      .pipe(gzip)
      .pipe(cipher)
      .pipe(appendInitVect)
      .pipe(writeStream)
      .on('close', () => {
        $('#message-bkp')
          .html(`${destName} saved`)

        $('.right img#waiting')
          .css('display', 'none')
      })
  })
}

$('#restore-crp').click(function () {
  dialog.showOpenDialog()
    .then(result => {
      if (result.filePaths) {
        restore(result.filePaths[0])
      }
    }).catch(err => {
      console.log(err)
    })
})

function restore (zipFile) {
  $('.right img#waiting')
    .css('display', 'block')

  dialogs.prompt('password', pwd => {
    const readIv = jetpack.createReadStream(zipFile, { end: 15 })
    let initVect
    readIv.on('data', (chunk) => {
      initVect = chunk
    })

    readIv.on('close', () => {
      jetpack.copy(userDataDir, 'user_data-' + timestamp)

      const readStream = jetpack.createReadStream(zipFile, { start: 16 })
      const cipherKey = crypt.getCipherKey(pwd)
      const decipher = crypt.makeDecipher(cipherKey, initVect)

      readStream
        .pipe(decipher)
        .pipe(gunzip())
        .pipe(tar.extract(userDataDir))

      $('#message-rst')
        .html(`${zipFile} restored`)

      $('.right img#waiting')
        .css('display', 'none')
    })
  })
}

$('#output-data-store').click(function () {
  const Nedb = require('./modules/db-nedb')
  var db = new Nedb()

  db.getAll('forwards', function (error, results) {
    if (error) {
      console.log(error)
    } else {
      var html = ''

      $.each(results, function (index, row) {
        html += '<div class="entryWrap">'
        html += `<h6>${dateUtils.convertUnixToMysql(row.dateTime)}</h6>`
        html += `<div class="entry">${row.textEntry}</div>`
        html += `<div class="tags">${row.tags}</div>`
        html += '</div>'
      })

      var file = '<!doctype html>'
      file += '<html>'
      file += '<head>'
      file += '<style>'
      file += '.tags { font-style: italic; }'
      file += '</style>'
      file += '</head>'
      file += `<body>${html}</body>`
      file += '</html>'

      dialog.showOpenDialog({
        properties: ['openDirectory']
      }).then(result => {
        if (result.filePaths) {
          const destinationFile = path.join(
            result.filePaths.toString(),
            global.settings.dataStore.description + '.html'
          )

          jetpack.write(destinationFile, file)
        }
      }).catch(err => {
        console.error(err)
      })
    }
  })
})
