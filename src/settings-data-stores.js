/* eslint-env */
const jsonUtils = require('./modules/jsonUtils')
const ds = require('./modules/settingsUtils')
const dataStores = ds.getDataStores()
const path = require('path')

buildFormDataStores(dataStores)

function buildFormDataStores (dataStores) {
  $.each(dataStores, function (key, val) {
    doRowDataStore(key, val)
  })

  // do a blank row for new records
  var emptyVals = {
    desc: '',
    data: '',
    active: ''
  }

  doRowDataStore(dataStores.length, emptyVals)

  // save all data stores with stats
  $('<input type="button" id="btnSaveDataStores" class="save" value="Save" />')
    .on('click', function () {
      saveDataStore()
    })
    .appendTo('#frmDataStores')
}

function doRowDataStore (key, val) {
  var tbl = `tbl-data-stores-${key}`
  var rowHeader = `tbl-data-stores-header-${key}`
  var rowKey = `ds-row-${key}`
  var col1 = `ds-row-${key}-col_1`
  var col2 = `ds-row-${key}-col_2`
  var col3 = `ds-row-${key}-col_3`
  var col4 = `ds-row-${key}-col_4`

  var headers = [
    'description',
    'json file',
    'stats',
    'selected',
    ''
  ]

  $(`<table id="${tbl}" class="data-input">`)
    .appendTo('#frmDataStores')

  $(`<tr class="row" id="${rowHeader}">`)
    .appendTo(`#${tbl}`)

  $.each(headers, function (k, v) {
    $('<td>')
      .html(`<h6>${v}</h6>`)
      .appendTo(`#${rowHeader}`)
  })

  $(`<tr class="rowDS" id="${rowKey}">`)
    .appendTo(`#${tbl}`)

  // description
  $(`<td id="${col1}">`)
    .appendTo(`#${rowKey}`)
    .add('<input type="text" class="inpDesc" />')
    .val(val.desc)
    .appendTo(`#${col1}`)

  // dataFile
  $(`<td id="${col2}">`)
    .appendTo(`#${rowKey}`)
    .add('<input type="text" class="inpData" />')
    .val(val.data)
    .appendTo(`#${col2}`)

  // stats enabled
  var chk = $(`<input type="checkbox" class="chk" id="chkStatsEnabled-${key}" >`)
  if (val.statsEnabled) {
    chk.prop('checked', true)
  }

  $(`<td id="${col4}">`)
    .appendTo(`#${rowKey}`)
    .add(chk)
    .appendTo(`#${col4}`)

  // stats
  if (val.statsEnabled) {
    doStats(tbl, key, val.stats)
  }

  // selected/active
  if (val.desc) {
    var chkd = ''
    if (val.selected) {
      chkd = 'checked'
    }

    $(`<td id="${col3}">`)
      .appendTo(`#${rowKey}`)
      .add(`<input type="radio" name="chosen" id="chkSelected-${key}" />`)
      .prop('checked', chkd)
      .val(key)
      .on('click', function () {
        saveDataStore()
      })
      .appendTo(`#${col3}`)
  }
}

function toggleStats (rowKey) {
  $('.rowStat').each(function (key, row) {
    var id = ($(row).attr('id'))
    var statHeader = `stats-rowHeader-${rowKey}`
    var statRow = `stat-ds-${rowKey}`

    if (id.indexOf(statHeader) !== -1 ||
      id.indexOf(statRow) !== -1) {
      $(row).show()
    }
  })
}

function doStats (tbl, key, stats) {
  var header = `stats-rowHeader-${key}`

  $('<tr class="rowStatTrigger"><td>show stats</td>')
    .on('click', function () {
      toggleStats(key)
    })
    .appendTo(`#${tbl}`)

  $(`<tr class="rowStat" id="${header}">`)
    .css('display', 'none')
    .appendTo(`#${tbl}`)

  // headings
  var headers = [
    'stat name',
    'start',
    'end',
    'time',
    'enabled'
  ]

  $.each(headers, function (k, v) {
    $('<td>')
      .html(`<h6>${v}</h6>`)
      .appendTo(`#${header}`)
  })

  $.each(stats, function (k, v) {
    doStatRow(tbl, key, k, v)
  })

  // blank row for a new stat
  var blank = {
    name: null,
    start: null,
    end: null,
    enabled: false
  }

  doStatRow(tbl, key, stats.length, blank)
}

function doStatRow (tbl, dsKey, statKey, val) {
  var comboKey = `${dsKey}-${statKey}`
  var rowKey = `stat-ds-${comboKey}`
  var col1 = `cell-ds-${comboKey}-col_1`
  var col2 = `cell-ds-${comboKey}-col_2`
  var col3 = `cell-ds-${comboKey}-col_3`
  var col4 = `cell-ds-${comboKey}-col_4`
  var col5 = `cell-ds-${comboKey}-col_5`

  $(`<tr class="rowStat" id="${rowKey}">`)
    .css('display', 'none')
    .appendTo(`#${tbl}`)

  // name
  $(`<td id="${col1}">`)
    .appendTo(`#${rowKey}`)
    .add($('<input type="text" class="inpName" />'))
    .val(val.name)
    .appendTo(`#${col1}`)

  // start
  $(`<td id="${col2}">`)
    .appendTo(`#${rowKey}`)
    .add($('<input type="text" class="inpStart" />'))
    .val(val.start)
    .appendTo(`#${col2}`)

  // end
  $(`<td id="${col3}">`)
    .appendTo(`#${rowKey}`)
    .add($('<input type="text" class="inpEnd" />'))
    .val(val.end)
    .appendTo(`#${col3}`)

  // time
  var chkTime = $('<input type="checkbox" class="chk time">')

  if (val.time) {
    chkTime.prop('checked', true)
  }

  $(`<td id="${col4}">`)
    .appendTo(`#${rowKey}`)
    .add(chkTime)
    .appendTo(`#${col4}`)

  // enabled
  var chkEnabled = $('<input type="checkbox" class="chk enabled">')

  if (val.enabled) {
    chkEnabled.prop('checked', true)
  }

  $(`<td id="${col5}">`)
    .appendTo(`#${rowKey}`)
    .add(chkEnabled)
    .appendTo(`#${col5}`)
}

function saveDataStore () {
  var dataRowsOnPage = getDataRowsOnPage()

  if (dataStores.length < dataRowsOnPage.length) {
    // new data store
    var lastIndex = dataRowsOnPage.length - 1
    var newJson = makeNewJsonFile(dataRowsOnPage[lastIndex])

    newJson.then(function (resultsMin) {
      var chosenTable = getChosenTable(dataRowsOnPage)
      var newDataStores = buildDataStoresArray(dataRowsOnPage, chosenTable)

      saveToDisc(newDataStores)
    })
  } else {
    saveToDisc(dataRowsOnPage)
  }
}

function saveToDisc (tableArray) {
  const settings = ds.getAllSettings()

  const newSettings = {
    dataStores: tableArray,
    themes: settings.themes
  }

  jsonUtils.saveData('settings.json', newSettings)
  const localWindow = electron.remote.getCurrentWindow()
  localWindow.reload()
}

function makeNewJsonFile (dataRow) {
  return new Promise(function (resolve, reject) {
    var jsonFile = dataRow.data
    var ext = jsonFile.split('.').pop()

    if (ext !== 'json') {
      $('.message').html('not json')
      return false
    }

    try {
      const Datastore = require('nedb')
      const dateUtils = require('./modules/dateUtils')
      const timeStamp = dateUtils.getNowUnix()
      const fileAndPath = path.join(__dirname, `../user_data/${jsonFile}`)

      const db = new Datastore({
        filename: fileAndPath,
        autoload: true
      })

      var doc = {
        dateTime: timeStamp,
        tags: '',
        statsEnabled: false,
        textEntry: 'A new repository'
      }

      db.insert(doc, function (err, newDoc) {
        if (err) {
          reject(err)
        } else {
          resolve(newDoc)
        }
      })
    } catch (e) {
      console.log('Failed to save the file!')
    }
  })
}

function getDataRowsOnPage () {
  var tables = []

  $('.data-input .rowDS').each(function (key, row) {
    var desc = $(row).find('.inpDesc').val()
    var data = $(row).find('.inpData').val()
    var selected = $(row).find(`#chkSelected-${key}`).prop('checked')
    var statsEnabled = $(row).find(`#chkStatsEnabled-${key}`).prop('checked')
    var stats = getStatsOnPage(key)

    if (desc !== '' & data !== '') {
      tables.push({
        desc: desc,
        data: data,
        selected: selected,
        statsEnabled: statsEnabled,
        stats: stats
      })
    }
  })

  return tables
}

function getStatsOnPage (key) {
  var statObjects = []

  $(`#tbl-data-stores-${key} .rowStat`).each(function (key, row) {
    var statName = $(row).find('.inpName').val()
    var statStart = $(row).find('.inpStart').val()
    var statEnd = $(row).find('.inpEnd').val()
    var statTime = $(row).find('.chk.time').prop('checked')
    var statEnabled = $(row).find('.chk.enabled').prop('checked')

    if (statName && statStart && statEnd) {
      statObjects.push({
        name: statName,
        start: statStart,
        end: statEnd,
        time: statTime,
        enabled: statEnabled
      })
    }
  })

  return statObjects
}

function buildDataStoresArray (tables, chosenTable) {
  var tNames = []

  $.each(tables, function (key, val) {
    var tName = {}

    if (val === chosenTable) {
      tName = {
        desc: val.desc,
        data: val.data,
        selected: true,
        statsEnabled: val.statsEnabled,
        stats: val.stats
      }
    } else {
      if (val.desc !== '' & val.data !== '') {
        tName = {
          desc: val.desc,
          data: val.data,
          statsEnabled: val.statsEnabled,
          stats: val.stats
        }
      }
    }

    tNames.push(tName)
  })

  return (tNames)
}

function getChosenTable (tblNames) {
  var indexOfChosenTable = $(':input:radio:checked').val()
  return (tblNames[indexOfChosenTable])
}
