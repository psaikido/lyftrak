/* eslint-env */
const entryBlock = require('./modules/buildEntryBlock.js')
const media = require('./modules/mediaUtils')
const jsonUtils = require('./modules/jsonUtils')
const dateUtils = require('./modules/dateUtils')
const Nedb = require('./modules/db-nedb')
var db = new Nedb()
var idCurrent = ''
var id = ''

const urlParams = new URLSearchParams(window.location.search)
var idCurrentFromGet = urlParams.get('id')

getEntry(id)

function getEntry (id) {
  db.getLatest(function (error, resultsLatest) {
    if (error) {
      console.error(error)
    } else {
      var idCurrent = resultsLatest[0]._id // default is latest record

      if (idCurrentFromGet !== null && idCurrentFromGet !== 'undefined') {
        idCurrent = idCurrentFromGet // called from another page
      }

      if (id) {
        idCurrent = id // called from the buttons
      }

      doAnniversaryPrompt()

      db.getOne(idCurrent, function (error, results) {
        if (error) {
          console.error(error)
        } else {
          if (results.length < 1) { return }

          var row = results[0]
          entryBlock.buildEntryBlock(
            row,
            media
          )

          buildButtons(row)
        }
      })
    }
  })
}

function doAnniversaryPrompt () {
  var nextAn = getNextAnniversary()

  if (nextAn) {
    var msg = `next anniversary: ${nextAn.desc} ${nextAn.date} (currently ${nextAn.elapsedYears})`

    if ($('.notification').length === 0) {
      $('<div class="notification">')
        .html(msg)
        .appendTo('body')
    }
  }
}

function getNextAnniversary () {
  const dates = require('../user_data/anniversaries.json')
  var sortedDates = jsonUtils.readAnniversaries(dates.anniversaries)
  var today = dateUtils.getTodayUnix()
  var msg

  for (var i = 0; i < sortedDates.length; i++) {
    var row = sortedDates[i]

    if (parseInt(row.timestamp) >= today) {
      msg = buildAnniversaryRecord(row)
      break
    }
  }

  if (!msg) {
    msg = buildAnniversaryRecord(sortedDates[0])
  }

  return msg
}

function buildAnniversaryRecord (row) {
  const dateFormat = require('dateformat')
  var age = dateUtils.getAge(row.originalDate)
  var msg = {
    desc: row.desc,
    date: dateFormat(row.mysql, 'dd mmm'),
    elapsedYears: age
  }
  return msg
}

function buildButtons (row) {
  const max = getMax()
  const min = getMin()
  idCurrent = row._id
  var currentDate = parseInt($(`#titleDateUnix_${idCurrent}`).text(), 10)

  max.then(function (resultsMax) {
    min.then(function (resultsMin) {
      // earliest
      $('<input type="button" class="btnEarliest" value="<<" accesskey="h" title="Alt+h" />')
        .appendTo('.buttons')
        .on('click', function () {
          jumpToRecord(resultsMin._id)
        })

      if (row.dateTime > resultsMin.dateTime) {
        // previous
        $('<input type="button" class="btnPrevious" value="<" accesskey="j" title="Alt+j" />')
          .appendTo('.buttons')
          .on('click', function () {
            db.getPrevious(currentDate, function (error, results) {
              if (error) {
                console.error(error)
              }

              jumpToRecord(results[0]._id)
            })
          })
      }

      if (row.dateTime < resultsMax.dateTime) {
        // next
        $('<input type="button" class="btnNext" value=">" accesskey="k" title="Alt+k" />')
          .appendTo('.buttons')
          .on('click', function () {
            db.getNext(currentDate, function (error, results) {
              if (error) {
                console.error(error)
              }

              jumpToRecord(results[0]._id)
            })
          })
      }

      // latest
      $('<input type="button" class="btnLatest" value=">>" accesskey="l" title="Alt+l" />')
        .appendTo('.buttons')
        .on('click', function () {
          jumpToRecord(resultsMax._id)
        })
    })
  })
}

function getMax () {
  return new Promise(function (resolve, reject) {
    db.getLatest(function (error, results) {
      if (error) {
        reject(error)
      } else {
        if (results.length < 1) {
          return false
        }

        resolve(results[0])
      }
    })
  })
}

function getMin () {
  return new Promise(function (resolve, reject) {
    db.getFirst(function (error, results) {
      if (error) {
        reject(error)
      } else {
        if (results.length < 1) {
          return false
        }

        resolve(results[0])
      }
    })
  })
}

function jumpToRecord (id) {
  $('.entryWrap').remove()
  idCurrentFromGet = null
  getEntry(id)
}

$('body').on('click', '.entry a', (event) => {
  event.preventDefault()
  const link = event.target.href
  require('electron').shell.openExternal(link)
})
