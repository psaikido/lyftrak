/* eslint-env */
const Db = require('./modules/db-nedb-dashboard')
const dateUtils = require('./modules/dateUtils')
const d3 = require('d3')

$.each(global.settings.dataStores, function (key, dataStore) {
  const db = new Db(dataStore.data)
  const container = `store-${key}`

  doHeading(dataStore, container)
  doCount(db, container)
  doEarliestRecord(db, container)

  /*
  if (dataStore.statsEnabled) {
    doStats(db, dataStore, container)
  }
  */
})

function doStats (db, dataStore, container) {
  db.getStats(function (error, docs) {
    if (error) {
      console.error(error)
    } else {
      var statArray = []

      $.each(dataStore.stats, function (key, val) {
        var statName = val.name

        var oStat = {
          name: statName,
          start: val.start,
          end: val.end,
          stats: []
        }

        for (var x = 0; x < docs.length; x++) {
          if (docs[x].stats[statName] > 0) {
            var statDate = dateUtils.convertUnixToMysqlShort(docs[x].dateTime)
            var statVal = docs[x].stats[statName]

            oStat.stats.push({
              date: statDate,
              val: statVal
            })
          }
        }

        statArray.push([oStat])
      })

      doCharts(statArray, container)
    }
  })
}

function doEarliestRecord (db, container) {
  db.getFirst(function (error, docs) {
    if (error) {
      console.error(error)
    } else {
      var earliestDate = dateUtils.convertUnixToMysqlShort(docs[0].dateTime)

      $(`<p>earliest: ${earliestDate}</p>`)
        .appendTo(`#${container}`)
    }
  })
}

function doCount (db, container) {
  db.getCount(function (error, count) {
    if (error) {
      console.error(error)
    } else {
      $(`<p>records: ${count}</p>`)
        .appendTo(`#${container}`)
    }
  })
}

function doHeading (dataStore, container) {
  $(`<div class="dash" id="${container}">`)
    .appendTo('#dashboard')
    .add(`<h6>${dataStore.desc}</h6>`)
    .appendTo(`#${container}`)
}

function doCharts (dataset, container) {
  // console.log(dataset)
  const width = 700
  const height = 200
  const margin = 5
  const padding = 5
  const adj = 30

  for (var x = 0; x < dataset.length; x++) {
    var data = dataset[x][0]
    if (data.stats.length === 0) {
      continue
    }

    var divId = `graph-${data.name}`

    $(`<div class="svg-container" id="${divId}">`)
      .appendTo(`#${container}`)

    var vb = '-' + adj + ' -' + adj + ' ' + (width + adj * 3) + ' ' + (height + adj * 3)
    const svg = d3.select(`#${divId}`)
      .append('svg')
      .attr('preserveAspectRatio', 'xMinYMin meet')
      .attr('viewBox', vb)
      .style('padding', padding)
      .style('margin', margin)
      .classed('svg-content', true)

    const slices = data.stats.slice(1)
    const xScale = d3.scaleTime().range([0, width])
    const yScale = d3.scaleLinear().rangeRound([height, 0])

    xScale.domain(d3.extent(slices, function (d) {
      return (d.date)
    }))

    yScale.domain([(0), d3.max(slices, function (c) {
      return d3.max(c.val, function (d) {
        return parseInt(d.val) + 4
      })
    })
    ])

    const yaxis = d3.axisLeft().scale(yScale)
    const xaxis = d3.axisBottom().scale(xScale)

    svg.append('g')
      .attr('class', 'axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xaxis)

    svg.append('g')
      .attr('class', 'axis')
      .call(yaxis)

    const line = d3.line()
      .x(function (d) { return xScale(d.date) })
      .y(function (d) { return yScale(parseInt(d.val)) })

    const lines = svg.selectAll('lines')
      .data(slices)
      .enter()
      .append('g')

    //console.log(slices)
    lines.append('path')
      .attr('d', function (d) {
        return line(parseInt(d.val))
      })
  }
}
