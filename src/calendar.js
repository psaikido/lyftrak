/* eslint-env */
const dateFormat = require('dateformat')
const dateUtils = require('./modules/dateUtils')
const Nedb = require('./modules/db-nedb')
var db = new Nedb()
const calendar = require('node-calendar')
const thisYear = dateFormat('yyyy')

const earliestYear = getEarliestEntryYear()
earliestYear.then(function (ey) {
  for (let yr = ey.year; yr <= thisYear; yr++) {
    var populatedDays = getPopulatedDays(yr)

    populatedDays.then(function (results) {
      var cal = new calendar.Calendar()
      var yearCalendar = cal.yeardatescalendar(yr)
      renderCalendar(yr, yearCalendar, results)
    })
  }
})

function renderCalendar (yr, yearCalendar, populatedDays) {
  var yearId = `year_${yr}`

  $(`<div class="yearWrap" id="${yearId}">`)
    .appendTo('body')
  var yearTitle = yr

  $('<h3>')
    .text(yearTitle)
    .appendTo(`#${yearId}`)

  for (var row = 0; row < yearCalendar.length; row++) {
    var rowOfMonths = yearCalendar[row]
    var rowOfMonthsId = `rowOfMonths_${yr}_${row}`

    $(`<div class="rowOfMonths" id="${rowOfMonthsId}">`)
      .appendTo(`#${yearId}`)

    for (var x = 0; x < rowOfMonths.length; x++) {
      var month = rowOfMonths[x]
      var monthTitle = dateFormat(month[1][0], 'mmm')

      var monthId = `month_${yr}_${row}_${x}`
      $(`<div class="month" id="${monthId}">`)
        .appendTo(`#${rowOfMonthsId}`)
      $('<h6>')
        .text(monthTitle)
        .appendTo(`#${monthId}`)

      for (var y = 0; y < month.length; y++) {
        var week = month[y]

        var weekId = `week_${yr}_${row}_${x}_${y}`
        $(`<div class="week" id="${weekId}">`)
          .appendTo(`#${monthId}`)

        for (var z = 0; z < week.length; z++) {
          var day = dateFormat(week[z], 'yyyy-mm-dd')

          if (!getIsDayInMonth(day, month)) {
            continue
          }

          var dayFoundId = isDayPopulated(day, populatedDays)

          if (dayFoundId != null) {
            var dayId = `day_${yr}_${row}_${x}_${y}_${z}`
            $(`<div class="day hilight" id="${dayId}">`)
              .text(dateFormat(day, 'dd'))
              .appendTo(`#${weekId}`)
              .on('click', { eventId: dayFoundId }, dayClicked)
          } else {
            dayId = `day_${yr}_${row}_${x}_${y}_${z}`
            $(`<div class="day" id="${dayId}">`)
              .text(dateFormat(day, 'dd'))
              .appendTo(`#${weekId}`)
          }
        }
      }
    }
  }
}

function getPopulatedDays (theYear) {
  return new Promise(function (resolve, reject) {
    var range = dateUtils.getUnixRangeFromYear(theYear)

    var termStart = { dateTime: { $gte: range.dtStartUnix } }
    var termEnd = { dateTime: { $lt: range.dtEndUnix } }
    var terms = { $and: [termStart, termEnd] }

    db.find(terms, function (error, results) {
      if (error) {
        reject(error.code)
      } else {
        resolve(results)
      }
    })
  })
}

function dayClicked (event) {
  ipcRenderer.send('getEntryFromId', event.data.eventId)
}

function isDayPopulated (dayFormatted, populatedDays) {
  for (var i = 0; i < populatedDays.length; i++) {
    var popDay = dateUtils.convertUnixToMysqlShort(populatedDays[i].dateTime)
    if (dayFormatted === popDay) {
      return populatedDays[i]._id
    }
  }

  return null
}

function getIsDayInMonth (day, month) {
  // The raw data from the node-calendar gives dates outside of
  // a month. This here suppresses those by comparing with
  // the second week, second day (indexes both '1').
  var thisMonth = dateFormat(month[1][1], 'mm')
  var thisDay = dateFormat(day, 'mm')
  if (thisMonth === thisDay) {
    return true
  }

  return false
}

function getEarliestEntryYear () {
  return new Promise(function (resolve, reject) {
    db.getFirst(function (error, results) {
      if (error) {
        reject(error.code)
      } else {
        var dt = results[0].dateTime
        var year = dateUtils.getYearFromUnix(dt)
        resolve({ year, dt })
      }
    })
  })
}
