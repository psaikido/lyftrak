/* eslint-env */
var headers = [
  '<meta charset="utf-8">',
  '<!-- https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP -->',
  '<meta http-equiv="Content-Security-Policy" content="default-src \'self\' \'unsafe-inline\'; script-src \'self\' filesystem \'unsafe-inline\' https://cdnjs.cloudflare.com/; img-src * \'self\' data: http://www.w3.org/">',
  '<meta http-equiv="X-Content-Security-Policy" content="default-src \'self\' \'unsafe-inline\'; script-src \'self\' filesystem \'unsafe-inline\' https://cdnjs.cloudflare.com/; img-src * \'self\' data: http://www.w3.org/">',
  '<link rel="stylesheet" href="../assets/css/solarized.css" type="text/css">',
  `<link rel="stylesheet" href="../assets/css/${global.settings.theme}.css" type="text/css">`,
  `<title>lyftrak - ${global.settings.dataStore.description}</title>`
]

for (var x = 0; x < headers.length; x++) {
  $(headers[x]).appendTo('.head')
}
