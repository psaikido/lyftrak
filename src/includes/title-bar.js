/* eslint-env */
const { BrowserWindow } = require('electron').remote
const ipcRenderer = require('electron').ipcRenderer
const menu = require('./includes/menu-data')

$('<div id="header">')
  .appendTo('body')

$('<div id="title-bar">')
  .appendTo('#header')

$('<div id="title">')
  .text(`lyftrak - ${global.settings.dataStore.description}`)
  .appendTo('#title-bar')

$('<div id="title-bar-btns">')
  .appendTo('#title-bar')

$('<button id="min-btn">-</button>')
  .appendTo('#title-bar-btns')

$('<button id="max-btn">+</button>')
  .appendTo('#title-bar-btns')

$('<button id="close-btn">x</button>')
  .appendTo('#title-bar-btns')

$('<div id="menu">')
  .appendTo('#header')

$.each(menu, function (index, item) {
  var modifiedLabel = ''
  var lbl = item.label.split('')
  $.each(lbl, function (i, letter) {
    if (letter.toLowerCase() === item.accel) {
      modifiedLabel += `<u>${letter}</u>`
    } else {
      modifiedLabel += letter
    }
  })

  $('<button class="menu-item">')
    .html(modifiedLabel)
    .click(function () {
      go(item.target)
    })
    .attr('accesskey', item.accel)
    .attr('id', `accesskey_${item.accel}`)
    .appendTo('#menu')
})

function go (src) {
  ipcRenderer.send('nav', src)
}

function init () {
  $('#min-btn').on('click', (e) => {
    var window = BrowserWindow.getFocusedWindow()
    window.minimize()
  })

  $('#max-btn').on('click', (e) => {
    var window = BrowserWindow.getFocusedWindow()
    if (window.isMaximized()) {
      window.unmaximize()
    } else {
      window.maximize()
    }
  })

  $('#close-btn').on('click', (e) => {
    var window = BrowserWindow.getFocusedWindow()
    window.close()
  })
}

document.onreadystatechange = () => {
  if (document.readyState === 'complete') {
    init()
  }
}
