/* eslint-env */
const template = [
  {
    label: 'All',
    target: './src/all.html',
    accel: 'a'
  },
  {
    label: 'Anniversaries',
    target: './src/anniversaries.html',
    accel: 'i'
  },
  {
    label: 'Backup',
    target: './src/backup.html',
    accel: 'b'
  },
  {
    label: 'Calendar',
    target: './src/calendar.html',
    accel: 'c'
  },
  {
    label: 'Dashboard',
    target: './src/dashboard.html',
    accel: 'd'
  },
  {
    label: 'Latest',
    target: './src/index.html',
    accel: 'l'
  },
  {
    label: 'New',
    target: './src/edit.html',
    accel: 'n'
  },
  {
    label: 'Search',
    target: './src/search.html',
    accel: 'r'
  },
  {
    label: 'Settings',
    target: './src/settings.html',
    accel: 't'
  }
]

module.exports = template
