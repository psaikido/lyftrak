/* eslint-env */
const dateUtils = require('./modules/dateUtils')
const tagUtils = require('./modules/tagUtils')
const entryBlock = require('./modules/buildEntryBlock')
const media = require('./modules/mediaUtils')
const Nedb = require('./modules/db-nedb')
var db = new Nedb()

$('#inpSearchTerm').focus()

// tags
$('#inpSearchTag')
  .append(
    $('<option />')
      .val('')
      .text('')
  )

const tags = tagUtils.getAllTags(db)
tags.then(function (results) {
  for (var i = 0; i < results.length; i++) {
    $('#inpSearchTag')
      .append(
        $('<option />')
          .val(results[i])
          .text(results[i])
      )
  }
}, function (err) {
  console.log(err)
})

$('#btnSave').click(function (event) {
  var whereClause = ''

  var dateClause = isDateSearch($('#inpDateStart'), $('#inpDateEnd'))
  if (typeof dateClause !== 'undefined') {
    whereClause = buildWhereClause(whereClause, dateClause)
  }

  var idClause = isIdSearch($('#inpSearchId'))
  if (typeof idClause !== 'undefined') {
    whereClause = buildWhereClause(whereClause, idClause)
  }

  var termClause = isTermSearch($('#inpSearchTerm'))
  if (typeof termClause !== 'undefined') {
    whereClause = buildWhereClause(whereClause, termClause)
  }

  var tagClause = isTagSearch($('#inpSearchTag'))
  if (typeof tagClause !== 'undefined') {
    whereClause = buildWhereClause(whereClause, tagClause)
  }

  var mediaClause = isMediaPixSearch($('#inpSearchMediaPix'))
  if (typeof mediaClause !== 'undefined') {
    whereClause = buildWhereClause(whereClause, mediaClause)
  }

  mediaClause = isMediaVidSearch($('#inpSearchMediaVids'))
  if (typeof mediaClause !== 'undefined') {
    whereClause = buildWhereClause(whereClause, mediaClause)
  }

  if (whereClause) {
    db.find(whereClause, function (error, results) {
      if (error) {
        console.log(error)
      } else {
        showResults(results)
      }
    })
  }
})

function buildWhereClause (existing, clause) {
  if (existing) {
    return ($.extend(existing, clause))
  } else {
    return (clause)
  }
}

function isDateSearch (start, end) {
  var dateStart = start.val()
  if (dateStart) {
    dateStart = dateUtils.convertMysqlToUnix(dateStart)
    var objStart = { dateTime: { $gte: dateStart } }

    var dateEnd = end.val()
    if (dateEnd) {
      dateEnd = dateUtils.convertMysqlToUnix(dateEnd)
      var objEnd = { dateTime: { $lte: dateEnd } }

      var objRange = { $and: [objStart, objEnd] }
      console.log(objRange)

      return (objRange)
    }
  }
}

function isIdSearch (elem) {
  var id = elem.val()
  if (id) {
    return ({ _id: id })
  }
}

function isTermSearch (elem) {
  var searchTerm = elem.val()
  if (searchTerm.length > 0) {
    var reg = new RegExp(searchTerm, 'i')
    return ({ textEntry: reg })
  }
}

function isTagSearch (elem) {
  var searchTag = elem.val()
  if (searchTag.length > 0) {
    var reg = new RegExp(searchTag, 'i')
    return ({ tags: reg })
  }
}

function isMediaPixSearch (elem) {
  var searchMedia = elem.prop('checked')
  if (searchMedia) {
    return (
      {
        $and: [
          { pix: { $exists: true } },
          { pix: { $ne: 'null' } },
          { pix: { $ne: '' } },
          { pix: { $regex: /([a-zA-Z0-9\s_\\.\-():])+(.jpg|.jpeg|.gif|.png)$/ } }
        ]
      }
    )
  }
}

function isMediaVidSearch (elem) {
  var searchMedia = elem.prop('checked')
  if (searchMedia) {
    return (
      {
        $and: [
          { pix: { $exists: true } },
          { pix: { $ne: 'null' } },
          { pix: { $ne: '' } },
          { pix: { $regex: /([a-zA-Z0-9\s_\\.\-():])+(.mp4)$/ } }
        ]
      }
    )
  }
}

function showResults (results) {
  if (results.length > 0) {
    $('div.entryWrap').remove()

    $.each(results, function (key, row) {
      entryBlock.buildEntryBlock(
        row,
        media
      )
    })
  } else {
    $('#message').text('no results')
  }
}

$('#datepickerIconStart').click(function () {
  $('#inpDateStart').datetimepicker({
    theme: 'dark',
    mask: '9999/19/39',
    format: 'Y-m-d',
    timepicker: false
  })

  $('#inpDateStart').datetimepicker('show')
})

$('#datepickerIconEnd').click(function () {
  $('#inpDateEnd').datetimepicker({
    theme: 'dark',
    mask: '9999/19/39',
    format: 'Y-m-d',
    timepicker: false,
    onShow: function (ct) {
      this.setOptions({
        minDate: $('#inpDateStart').val() ? $('#inpDateStart').val() : false
      })
    }
  })

  $('#inpDateEnd').datetimepicker('show')
})
