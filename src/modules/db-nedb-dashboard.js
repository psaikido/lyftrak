/* eslint-env */
const Datastore = require('nedb')
const path = require('path')

module.exports = class Nedb {
  constructor (dataFile) {
    const flatfile = path.join(
      __dirname,
      `../../user_data/${dataFile}`
    )

    this.db = new Datastore({
      filename: flatfile,
      autoload: true
    })
  }

  getCount (callback) {
    this.db.count({}, function (err, count) {
      callback(err, count)
    })
  }

  getFirst (callback) {
    this.db.find({})
      .sort({ dateTime: 1 })
      .limit(1)
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }

  getStats (callback) {
    this.db.find({})
      .sort({ dateTime: 1 })
      .projection({
        dateTime: 1,
        stats: 1
      })
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }
}
