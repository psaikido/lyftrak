/* eslint-env */
const Datastore = require('nedb')
const path = require('path')
const flatfile = path.join(
  __dirname,
  `../../user_data/${global.settings.dataStore.dataFile}`
)

module.exports = class Nedb {
  constructor () {
    this.db = new Datastore({
      filename: flatfile,
      autoload: true
    })
  }

  getAll (direction, callback) {
    var timeDirection
    if (direction === 'backwards') {
      timeDirection = -1
    } else {
      timeDirection = 1
    }

    this.db.find({})
      .sort({ dateTime: timeDirection })
      .projection({
        dateTime: 1,
        pix: 1,
        textEntry: 1,
        tags: 1,
        _id: 1
      })
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }

  getTagsUnique (callback) {
    this.db.find(
      {
        $and: [
          { tags: { $exists: true } },
          { tags: { $ne: 'null' } },
          { tags: { $ne: '' } }
        ]
      },
      { tags: 1 },
      function (err, docs) {
        callback(err, docs)
      })
  }

  getOne (id, callback) {
    this.db.find({ _id: id }, function (err, docs) {
      callback(err, docs)
    })
  }

  getCount (callback) {
    this.db.count({}, function (err, docs) {
      callback(err, docs)
    })
  }

  getLatest (callback) {
    this.db.find({})
      .sort({ dateTime: -1 })
      .limit(1)
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }

  getFirst (callback) {
    this.db.find({})
      .sort({ dateTime: 1 })
      .limit(1)
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }

  getNext (dt, callback) {
    this.db.find({
      dateTime: { $gt: dt }
    })
      .sort({ dateTime: 1 })
      .limit(1)
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }

  getPrevious (dt, callback) {
    this.db.find({
      dateTime: { $lt: dt }
    })
      .sort({ dateTime: -1 })
      .limit(1)
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }

  update (rec, callback) {
    this.db.update(
      { _id: rec._id },
      {
        $set: {
          dateTime: rec.dateTime,
          stats: rec.stats,
          pix: rec.pix,
          tags: rec.tags,
          textEntry: rec.textEntry
        }
      },

      function (err, docs) {
        callback(err, docs)
      }
    )
  }

  updateFields (id, opts, callback) {
    this.db.update(
      { _id: id },
      { $set: opts },

      function (err, docs) {
        callback(err, docs)
      }
    )
  }

  insert (rec, callback) {
    this.db.insert(rec, function (err, docs) {
      callback(err, docs)
    })
  }

  remove (id, callback) {
    this.db.remove({ _id: id }, function (err, docs) {
      callback(err)
    })
  }

  find (terms, callback) {
    this.db.find(terms)
      .sort({ dateTime: -1 })
      .exec(function (err, docs) {
        callback(err, docs)
      })
  }
}
