/* eslint-env */
module.exports = {
  doMedia: function (
    idCurrent,
    pix,
    mode
  ) {
    if (pix) {
      var allowedExtensions = [
        'jpg',
        'jpeg',
        'png',
        'gif',
        'mp4'
      ]

      pix = pix.split(',')

      const upPath = global.settings.dataStore.uploadDir
      const entryId = `#entry_${idCurrent}`
      const wrapDivId = `pix_wrap_${idCurrent}`

      $(`<div id="${wrapDivId}">`)
        .insertAfter(entryId)

      $.each(pix, function (key, pic) {
        var ext = pic.split('.').pop()

        if ($.inArray(ext, allowedExtensions) > -1) {
          var divId = `pix_${idCurrent}_${key}`

          if (ext === 'mp4') {
            $('<div class="pix" id="' + divId + '">')
              .html('<video width="480" height="320" controls="controls" id="mp4_' + divId + '">')
              .appendTo(`#${wrapDivId}`)

            $('<source src="' + upPath + pic + '" type="video/mp4">')
              .appendTo('#mp4_' + divId)
          } else {
            $('<div class="pix" id="' + divId + '">')
              .html('<img src="' + upPath + pic + '" />')
              .appendTo(`#${wrapDivId}`)
          }

          if (mode === 'edit') {
            $('<h6>')
              .text(pic)
              .appendTo('#' + divId)

            $('<input type="button" value="Delete">')
              .on('click', function () {
                deletePic(idCurrent, pic)
              })
              .appendTo('#' + divId)
          }
        }
      })
    }
  },

  getExistingPix: function (idCurrent) {
    return new Promise(function (resolve, reject) {
      const Nedb = require('./db-nedb')
      var db = new Nedb()

      db.find({ _id: idCurrent }, function (error, results, fields) {
        if (error) {
          reject(error.code)
        } else {
          if (results.length < 1) {
            return false
          }

          resolve(results[0].pix)
        }
      })
    })
  }
}

function deletePic (idCurrent, pic) {
  const electron = require('electron')
  const ipcRenderer = electron.ipcRenderer
  const jetpack = require('fs-jetpack')
  const mediaUtils = require('./mediaUtils')

  var existingPix = mediaUtils.getExistingPix(idCurrent)
  existingPix.then(function (results) {
    var existingPixArray = results.split(',')
    var pixUpdateString = ''

    for (var x = 0; x < existingPixArray.length; x++) {
      if (pic !== existingPixArray[x]) {
        if (pixUpdateString.length > 0) {
          pixUpdateString += ',' + existingPixArray[x]
        } else {
          pixUpdateString = existingPixArray[x]
        }
      }
    }

    const Nedb = require('./db-nedb')
    var db = new Nedb()
    db.updateFields(idCurrent, { pix: pixUpdateString }, function (error, results) {
      if (error) {
        console.log(error)
      } else {
        var dest = global.settings.dataStore.uploadDir + pic
        jetpack.remove(dest)
      }
    })

    ipcRenderer.send('getEntryFromId', idCurrent)
  })
}
