/* eslint-env */
module.exports = {
  buildEntryBlock: function (
    row,
    media
  ) {
    const idCurrent = row._id

    doHeader(row)
    doButtons(idCurrent)
    doText(row)
    media.doMedia(idCurrent, row.pix)

    doStats(row)
    doTags(row)

    $('<div class="clearDiv">&nbsp;</div>')
      .appendTo(`#entry_${idCurrent}`)
  }
}

function doHeader (row) {
  const dateUtils = require('./dateUtils')
  const idCurrent = row._id

  var entryWrapId = 'entryWrap_' + idCurrent
  var headerId = 'header_' + idCurrent
  var titleDateId = 'titleDate_' + idCurrent
  var titleDateUnixId = 'titleDateUnix_' + idCurrent
  var headerTextId = 'headerText_' + idCurrent
  var titleInfoId = 'titleInfo_' + idCurrent
  var titleIdId = 'titleId_' + idCurrent

  var entryDateHuman = dateUtils.convertUnixToMysql(row.dateTime)
  if (entryDateHuman.indexOf('00:00') !== -1) {
    entryDateHuman = entryDateHuman.replace(' 00:00', '')
  }

  $(`<div class="entryWrap" id="${entryWrapId}">`)
    .appendTo('body')
  $(`<div class="header" id="${headerId}">`)
    .appendTo(`#${entryWrapId}`)
  $(`<div class="titleDate" id="${titleDateId}">`)
    .text(entryDateHuman)
    .appendTo(`#${headerId}`)
  $(`<div class="titleDateUnix" id="${titleDateUnixId}">`)
    .text(row.dateTime)
    .appendTo(`#${headerId}`)
  $(`<div class="titleInfo" id="${titleInfoId}">`)
    .appendTo(`#${headerId}`)
  $(`<span class="headerText" id="${headerTextId}">`)
    .text('id: ')
    .appendTo(`#${titleInfoId}`)
  $(`<span class="titleIdHeader" id="${titleIdId}">`)
    .text(idCurrent)
    .appendTo(`#${headerTextId}`)
}

function doButtons (idCurrent) {
  const electron = require('electron')
  const ipcRenderer = electron.ipcRenderer

  var headerId = 'header_' + idCurrent
  var buttonsId = 'buttons_' + idCurrent
  var btnEditId = 'btnEdit_' + idCurrent
  var btnViewId = 'btnView_' + idCurrent

  $(`<div class="buttons" id="${buttonsId}">`)
    .insertAfter(`#${headerId}`)

  // edit
  $(`<button class="btnEdit" id="${btnEditId}" />`)
    .html('<u>E</u>dit')
    .attr('accesskey', 'e')
    .attr('title', 'Alt+e')
    .appendTo(`#${buttonsId}`)
    .on('click', function () {
      ipcRenderer.send('editEntry', idCurrent)
    })

  // view
  if (window.location.pathname.indexOf('index.html') === -1) {
    $(`<button class="btnView" id="${btnViewId}" />`)
      .html('<u>V</u>iew')
      .attr('accesskey', 'v')
      .attr('title', 'Alt+v')
      .appendTo(`#${buttonsId}`)
      .on('click', function () {
        ipcRenderer.send('getEntryFromId', idCurrent)
      })
  }
}

function doText (row) {
  const idCurrent = row._id

  var buttonsId = 'buttons_' + idCurrent
  var entryId = 'entry_' + idCurrent
  $(`<div class="entry" id="${entryId}">`)
    .html(row.textEntry)
    .insertAfter(`#${buttonsId}`)
}

function doTags (row) {
  if (row.tags) {
    const idCurrent = row._id

    var tagsId = 'tags_' + idCurrent
    var entryId = 'entry_' + idCurrent

    $(`<div class="tags" id="${tagsId}">`)
      .appendTo(`#${entryId}`)
    $('<span class="tagsDisplay">')
      .text(`#${row.tags}`)
      .appendTo(`#${tagsId}`)
  }
}

function doStats (row) {
  if (row.stats) {
    var score = 0

    for (var s in row.stats) {
      if (row.stats[s]) {
        score += parseInt(row.stats[s])
      }
    }

    if (score > 0) {
      $(`<div class="stats" id="stats_${row._id}">`)
        .appendTo(`#entryWrap_${row._id}`)

      for (var i = 0; i < global.settings.dataStore.stats.length; i++) {
        var stat = global.settings.dataStore.stats[i]
        doStat(stat, row)
      }
    }
  }
}

function doStat (stat, row) {
  const dateUtils = require('./dateUtils')

  if (stat.enabled) {
    var currentVal = 0

    if (row) {
      for (var s in row.stats) {
        if (stat.name === s) {
          currentVal = row.stats[s]
          break
        }
      }

      if (currentVal > 0) {
        var statId = `${stat.name}_${row._id}`
        var statBlockId = `${stat.name}Block_${row._id}`

        if (stat.time) {
          var time = dateUtils.convertMinsToHuman(currentVal)

          $(`<div class="stat" id="${statId}">`)
            .html(`<h6>${stat.name}: ${time}</h6>`)
            .appendTo(`#stats_${row._id}`)
        } else {
          $(`<div class="stat" id="${statId}">`)
            .html(`<h6>${stat.name}: ${currentVal} / ${stat.end}</h6>`)
            .appendTo(`#stats_${row._id}`)

          $(`<div class="statBlockWrap" id="${statBlockId}">`)
            .appendTo(`#${statId}`)

          var blockWidth = 600 / stat.end

          for (var x = 1; x <= currentVal; x++) {
            $(`<div class="statBlock" style="width: ${blockWidth}px">`)
              .appendTo(`#${statBlockId}`)
          }
        }
      }
    }
  }
}
