/* eslint-env */
module.exports = {
  getAllSettings () {
    const settings = require('../../user_data/settings')
    const globalSettings = {
      dataStore: this.getCurrentDataSettings(),
      dataStores: settings.dataStores,
      theme: this.getCurrentTheme(),
      themes: settings.themes
    }

    return globalSettings
  },

  getStats (idCurrent, db) {
    return new Promise(function (resolve, reject) {
      db.getOne(idCurrent, function (error, results) {
        if (error) {
          reject(error.code)
        } else {
          resolve(results[0])
        }
      })
    })
  },

  getDataStores () {
    const settings = require('../../user_data/settings')
    return settings.dataStores
  },

  getCurrentDataSettings () {
    const path = require('path')
    const settingsData = require('../../user_data/settings')
    var dataStores = settingsData.dataStores

    for (var i = 0; i < dataStores.length; i++) {
      var ds = dataStores[i]

      if (ds.selected) {
        const uploadDir = path.join(
          __dirname,
          '../../',
          'user_data/uploads',
          path.parse(ds.data).name,
          '/'
        )

        var currentDataStore = {
          description: ds.desc,
          dataFile: ds.data,
          uploadDir: uploadDir,
          stats: ds.stats
        }
      }
    }

    return currentDataStore
  },

  getThemeSettings () {
    const settings = require('../../user_data/settings')
    return settings.themes
  },

  getCurrentTheme () {
    const settings = require('../../user_data/settings')
    var currentTheme = ''

    for (var i = 0; i < settings.themes.length; i++) {
      var t = settings.themes[i]

      if (t.active) {
        currentTheme = t.name
        break
      }
    }

    return currentTheme
  }
}
