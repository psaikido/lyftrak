/* eslint-env */
const crypto = require('crypto')

module.exports = class CryptUtils {
  constructor () {
    this.key = ''
    this.initVect = ''
  }

  makeCipher (pwd) {
    const a = 'aes-256-cbc'
    this.key = Buffer.alloc(32)
    this.key = Buffer.concat([Buffer.from(pwd)], this.key.length)

    this.initVect = crypto.randomBytes(16)
    this.initVect = Buffer.from(
      Array.prototype.map.call(this.initVect, () => {
        return Math.floor(Math.random() * 256)
      })
    )

    return crypto.createCipheriv(a, this.key, this.initVect)
  }

  makeDecipher (cipherKey, initVect) {
    const a = 'aes-256-cbc'
    return crypto.createDecipheriv(a, cipherKey, initVect)
  }

  getCipherKey (pwd) {
    return crypto.createHash('sha256').update(pwd).digest()
  }
}
