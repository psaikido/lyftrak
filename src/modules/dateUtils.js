/* eslint-env */
const dateFormat = require('dateformat')

module.exports = {
  convertUnixToMysql: function (unixTime) {
    var dt = new Date(unixTime * 1000)
    return (dateFormat(dt, 'yyyy-mm-dd HH:MM'))
  },

  convertUnixToMysqlShort: function (unixTime) {
    var dt = new Date(unixTime * 1000)
    return (dateFormat(dt, 'yyyy-mm-dd'))
  },

  convertMysqlToUnix: function (mysqlDate) {
    var timestamp = new Date(mysqlDate.replace(' ', 'T')).getTime() / 1000
    return timestamp
  },

  getNowMysql: function () {
    var now = Date.now()
    return dateFormat(now, 'yyyy-mm-dd HH:MM')
  },

  getNowMysqlShort: function () {
    var now = Date.now()
    return dateFormat(now, 'yyyy-mm-dd')
  },

  getNowUnix: function () {
    var timeStamp = Math.round((new Date()).getTime() / 1000)
    return timeStamp
  },

  getTodayUnix: function () {
    var start = new Date()
    start.setHours(0, 0, 0, 0)
    const startOfDay = Math.floor(start.getTime() / 1000)
    return startOfDay
  },

  getYearFromUnix: function (dt) {
    dt = new Date(dt * 1000)
    return dt.getFullYear()
  },

  getUnixRangeFromYear: function (dt) {
    var dtStart = new Date(dt + '/01/01 00:00:00')
    var dtStartUnix = dtStart.getTime() / 1000

    var dtEnd = new Date((dt + 1) + '/01/01 00:00:00')
    var dtEndUnix = dtEnd.getTime() / 1000

    return { dtStartUnix, dtEndUnix }
  },

  convertMinsToHuman: function (mins) {
    var hours = pad2(Math.floor(mins / 60))
    var mns = pad2(Math.floor(mins % 60))
    return `${hours}:${mns}`
  },

  getAge: function (incomingDate) {
    var now = this.getNowMysqlShort().split('-')
    var incdt = incomingDate.split('-')

    // is the incoming date before today?
    // ie. has the anniversary come yet
    var aniHappened = false

    if (now[1] > incdt[1]) {
      // is this year's month later than the anniversary's month?
      aniHappened = true
    } else if (now[1] === incdt[1]) {
      // we have the same month so check the day
      if (now[2] >= incdt[2]) {
        aniHappened = true
      }
    }

    if (aniHappened) {
      return now[0] - incdt[0]
    } else {
      return now[0] - incdt[0] - 1
    }
  }
}

function pad2 (number) {
  return (number < 10 ? '0' : '') + number
}
