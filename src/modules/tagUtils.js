/* eslint-env */
module.exports = {
  getAllTags: function (db) {
    return new Promise(function (resolve, reject) {
      var tmp = ''

      db.getTagsUnique(function (error, results) {
        if (error) {
          reject(error)
        } else {
          var ret = []

          for (var i = 0; i < results.length; i++) {
            tmp = results[i]
            tmp = tmp.tags.split(',')

            if (tmp.length > 1) {
              for (var x = 0; x < tmp.length; x++) {
                if (isUnique(tmp[x].trim(), ret)) {
                  ret.push(tmp[x].trim())
                }
              }
            } else {
              if (isUnique(tmp[0].trim(), ret)) {
                ret.push(tmp[0].trim())
              }
            }
          }

          resolve(
            ret.sort(function (a, b) {
							if (a.toLowerCase() < b.toLowerCase()) return -1
							if (a.toLowerCase() > b.toLowerCase()) return 1
							return 0
            })
          )
        }
      })
    })
  }
}

function isUnique (proposal, masterList) {
  if ($.inArray(proposal, masterList) === -1) {
    return proposal
  }
}
