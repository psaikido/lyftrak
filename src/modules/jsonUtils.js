/* eslint-env */
module.exports = {
  saveData (file, values) {
    const fs = require('fs')
    const data = JSON.stringify(values, null, 4)

    var dest = process.env.HOME
    dest = dest.toString() + '/lyftrak/user_data/' + file

    try {
      fs.writeFile(dest, data, (err) => {
        if (err) {
          throw err
        } else {
          return true
        }
      })
    } catch (e) {
      console.log('Failed to write to' + dest + ' :: ' + e)
    }
  },

  readAnniversaries (anis) {
    const dateFormat = require('dateformat')
    const dateUtils = require('./dateUtils')
    const thisYear = dateFormat('yyyy')
    var retAnis = []

    for (var i = 0; i < anis.length; i++) {
      var incomingDate = anis[i].baseDate
      var baseDate = new Date(incomingDate)
      var dayOfYear = dateFormat(incomingDate, 'mm-dd')
      var ani = thisYear + '-' + dayOfYear + ' 00:00:00'
      var aniUnix = dateUtils.convertMysqlToUnix(ani)

      retAnis.push(
        {
          mysql: baseDate,
          originalDate: incomingDate,
          timestamp: aniUnix,
          desc: anis[i].desc
        }
      )
    }

    retAnis.sort(function (a, b) {
      var t1 = parseInt(a.timestamp)
      var t2 = parseInt(b.timestamp)

      return t1 < t2 ? -1 : 1
    })

    return retAnis
  }
}
