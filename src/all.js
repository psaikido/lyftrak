/* eslint-env */
const entryBlock = require('./modules/buildEntryBlock')
const media = require('./modules/mediaUtils')
const Nedb = require('./modules/db-nedb')
var db = new Nedb()

db.getAll('forwards', function (error, results) {
  if (error) {
    console.log(error)
  } else {
    $.each(results, function (index, row) {
      entryBlock.buildEntryBlock(
        row,
        media
      )
    })
  }
})
