/* eslint-env */
const electron = require('electron')
const themeUtils = require('./modules/settingsUtils')
const themes = themeUtils.getThemeSettings()

buildFormThemes(themes)

function buildFormThemes (themes) {
  $('<table id="themesTable" class="data-input">')
    .appendTo('#frmThemes')

  $.each(themes, function (key, val) {
    doRowThemes(key, val)
  })

  // save button
  $('<input type="button" value="Save" class="save" id="btnSaveThemes" />')
    .insertAfter('#themesTable')
}

function doRowThemes (key, val) {
  var rowKey = `themes_row_${key}`
  var col1 = `theme_${key}_col_1`
  var col2 = `theme_${key}_col_2`

  $(`<tr class="row" id="${rowKey}">`)
    .appendTo('#themesTable')

  $(`<td id="${col1}">`)
    .text(val.name)
    .appendTo(`#${rowKey}`)

  var chk = $('<input type="radio" name="themeNames" class="chk">')

  if (val.active) {
    chk.prop('checked', true)
  }

  $(`<td id="${col2}">`)
    .appendTo(`#${rowKey}`)
    .add(chk)
    .appendTo(`#${col2}`)
}

$('#btnSaveThemes').on('click', () => {
  var rows = getRows()
  const settings = themeUtils.getAllSettings()
  const newSettings = {
    stats: settings.stats,
    dataStores: settings.dataStores,
    themes: rows
  }

  jsonUtils.saveData('settings.json', newSettings)

  const localWindow = electron.remote.getCurrentWindow()
  localWindow.reload()
})

function getRows () {
  var recs = []

  $('#themesTable .row').each(function (key, row) {
    var name = $(row).find('td').html()
    var active = $(row).find('.chk').prop('checked')

    if (name) {
      recs.push({
        name: name,
        active: active
      })
    }
  })

  return recs
}
